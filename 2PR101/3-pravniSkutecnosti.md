# Úvod
**Definice právní skutečnosti**

  - Právně významné skutečnosti, které vyvolávají právní nádledky, tj. vznik, změnu nebo zánik práv a povinnosti

> Právní názledky -> vznik, změna nebo zánik práv a povinností

# Druhy právních skutečností

  - Jsou projevem vůle
    - Právní jednáni (aprobované)
    - Protiprávní jednání (probované)
  - Nejsou projevem vůle
    - Právní události
        - Příkladem je dosažení věku 18. Ať osoba chce či nechce dosahne nějakých práv a povinností
    - Konstututivní rozhodnutí OVM
    - Protiprávní stavy

## Právní jednání
### Pojem

  - Je to takové jednání (chování) osoby, jež je **způsobilé vyvolat právní následky**, tedy vznik, změnu nebo zánik práv a povinností.

Právní následky jsou **vyjádřeny**:

1. V samozném právním jednání (např. ve smlouvě)
2. V zákoně
3. Vyplývají z dobrých mravů
4. Ze zvykloti
5. Zavedené praxe stran

### Druhy právního jednání

  - Jednostranné X dvou- a vícestranné
    - Typické dvoustranné jednání je smlouva
    - Jednostranné je odstoupení od smlouvy
  - Adresované X Neadresované
    - Adresovaná směřují vůči konkrétní osobě. Např. návrh na uzavření smlouvy
  - Mezi živými X pro případ smrti
    - Pro případ smrti např. dědická smlouva
  - Synallagmatické X Asynallagmatické
    - Syn. je vzájemné právní jednání, které závísí na vzájemném protiplnění. Např. uzavření kupní smlouvy
    - Asyn. Např. darování. Není požadované protiplnění
  - Úplatné X Bezplatné
  - Pojmenované X Nepojmenované
    - Vztahuje se ke smlouvám
    - Také se nazývájí typová a netypová NEBO nominátní a inominátní
    - Pojmenováné smlouvy lze přímo najít v zákoníku. Např. licenční smlouva, kupní smlouva a t.d.
    - Nepojmenovaná je třeba leasingová smlouva

### Náležitosti právního jednání

**4 základní náležitosti:**

1. Náležitost osoby
    - Možnost zasoupení jinou osobou
    - Právní osobnost
        - Musí být **osobou v právním smyslu**
    - Svéprávnost
        - Musí být **způsobilá k danému právnímu jednání**
2. Náležitost vůle

> Vůle je psychický vztah jednajícího člověka k zamýšlenému (chtěnému) násůedku

Chybí-li vůle jednajícíc osoby nejde o právní jednání

Musí být:

  - Svobodná vůle
  - Vážná vůle
    - Nesmí být učiněná vůči žertu
  - Prostá omylu (rozhodujícíc/vedlejší okolnost, lest)

3. Náležitost projevu vůle

    - Určitý projev vůle
    - Srozumitelný projev vůle
    - Náležitá (stanovená) forma

> O právní jednání nejde, nelze-li **pro neurčitost nebo nesrozumitelnost** zjistit jeho obsah ani výkladem.

4. Náležitost předmětu vůle

    - Fyzická možnost
    - Právní dovolenost

### Jednání s vadami

1. Zdánlivé právní jednání

    - Jednání, které vůbec jednáním právním není, a tedy **nevyvolává právní následky**
    - Ke zdánlivému jednání "se nepřihlíží"

**4 případy:**

  1. Chybí vůlu jednající osoby (např. pro fyzické násilí)
  2. Nebyla zjevně projevena vážná vůle
  3. Vada v projevu vůle: nesrozumitelnost/neurčitost
  4. Právní jednání ještě nebylo dovršeno (imperfekce)

2. Neplatné právní jendání
    - Absolutně neplatné právní jednání
        1. Zjevně se příčí dobrým mravům
        2. Odporuje zákonu a zjevně nerušuje veřejný pořádek
        3. Zavazuje k plnění od počátku nemožnému

        - Sankce neplatnosti na ochranu "celé společnosti"
        - Neplatnosti se může dovolat kdokoli
        - SOud k neplatnosti přihlédne i bez návrhu (*ex offo*)
    - Relativně neplatné jednání
        - Právní jednání je platné až do doby, kdy se jeho neplatnosti dovolá strana, která je chráněna (tj. namítne neplatnost u soudu)
        - Sankce na ochranu určité osoby
        - SOud sám ze své vůle **nemůže** dané jednání prohlásit za neplatné, dovolat se neplatnosti může jen daná osoba

      Platí pro případy neplatnosti, kde nejsou naplněny podmínky pro absolutní neplatnost

3. Relativně neůčinné právní jednání
    - Jednání je sice platné, ale (pouze) proti konkrétní osobě (věřiteli nebo v případě úpadku vůči majetkové podstatě) neúčinné
    - Rozhoduje soud na základě **tzv. odpůrčí žaloby** v případě, že jednání dlužníka zkracuje uspokojení vykonatelné pohledávky věřitele

## Právní události

  - Právní události jsou takové právně relevantní události, které jsou nezávislé na lidské vůli a vyvolávají právní následky
    - Vznik, změnu nebo zánik práv a povinností

Jaké právní následky právní události vyvolá
1. Stanoveno zákonem
2. Ujednáno mezi stranami

### Příklady

  - Uplynutí času
    - Vydržení vlastnického práva
    - Promlčení a prekluze
  - Dovršení věku
    - Vznik práva být volen
    - Vznik práva na starobní důchod
  - Úraz
    - Vznik povinnosti nahradit nemajetkovou újmu
    - Vznik práva na plnění z pojistné smlouvy
  - Smrt
    - Zánik manželství
    - Zánik pracovněprávního poměru
  - Narození
    - Vznik práva a povinností plynoucích z občanství
    - Vznik práva na rodičovskou péči
    - Vznik práva na ochranu zdraví

### Doby a lhůty

**Doba** - časový úsek, jehož uplynutím bez dalšího zaniká právo nebo povinnost

**Lhůta** - časový úsek stanovený k uplatnění práva dané osoby u:

  1. Druhé strany
  2. Soudu
  3. Jiného orgánu veřejné moci

#### Lhůty
##### Promlčencí X prekluzivní lháta

**Promlčecí lhůta**

  - Jejím uplynutí právo zaniká, ale eixstuje v naturální podobě
  - Pokud dlužník namítne promlčení u soudu, je právo promlčené a nemusí plnit
  - Pokud však plnil, nemá nárok na vrácení
  - Promlčují se:
    - **Majetková práva** - promlčují se všechna s výjimkou případů stanovených zákonem
    - **Jiná práva** - 

**Prekluzivní lhůta**

  - Jejím uplynutím právo zaniká
  - Dlužník prekluzi nemusí namítat, soud k ni přihlédne i z moci úřední
  - Pokud by dlužník plnil, měl by nárok na vrácení bezdůvodného obohacení (věřitel si plnění nemůže nechat)

  - **Příklad:**
    - "Vyloučený člen může do trí měsíců od doručení konečného rozhodnutí solku o svém vyloučení navrhnout soudu, aby rozhodl o neplatnosti vyloučení; jinak toto právo zaniká. Nebylo-li mu rozhodnutí doručeno, může člen návrh podat do tří měsíců ode dne, kdy se o něm dozvěděl, nejdéle však do jednoho orku ode dne, kdy byl po vydání rozhodnutí zánik jeho členství vyloučením zapsán do seznamu členů; jinak toto právo zaniká"

##### Subjektivní X objektivní lhůty

Délku lhůt stanoví zákon (strany si je mohou v rámci zákonného omezení upravit)

Rozhodující je, která ze lhůt uplyne jako první

**Subjektivní lhůta**

  - Začíná běžet od okamžiku, kdy se daná osoba dozvěděla o rozhodné skutečnosti

**Objektivní lhůta**

  - Začíná běžet od okamžika, kdy k rozhodné skutečnosti došlo

##### Hmotněprávní X procesní lhůty

**Hmotněprávní lhůty**

  - Lhůty k uplatnění práva podle hmotněprávních předpisů (např Občanský zákoník)
  - Změškání nelze prominout: práva buď zanikaji (perkluze) nebo jim není přiznána soudní ochrana (promlčení)
  - Rozhodné včasné dojití

**Procesní lhůty**

  - TODO Missing

#### Pravidla pro počítání času

|  Událost   |   Čas   |
| ---- | ---- |
| Vznik/nabytí práva/povinnosti | Počátek dne (0.00 hod) |
| Zánik práva / povinnosti | Konec dne (24.00 hod) |
| Zánik práva jedné strany -> vznik práva druhé strany | Konec dne (24.00 hod) |
| Konec lhůty/doby pro výkon práva nebo splnění povinnosti | Obvyklá denní doba posledního dne (podnikatel - provozní doba) |

#### Počítání dob a lhůt

| Jednotka | Popis |
| ---- | ---- |
| Určení v jednotkách menších než den (hodiny, minuty) | Od okamžiku kdy začne, do okamžiku kdy skončí |
| Den | Ode dne, který následje po skutečnosti rozhodné pro její počátek |
| Týde, měsíc, rok | Konec v den, který se pojmenováním nebo číslem shoduje se dnem, kdy nastala skutečnost rozhodná pro její počátek |

> Pouze pro lhůty (nikoli doby)

**Příklad 1**

V rámci platně uzavřené smlouvy si obchodní partneří ujednali, že nedostatky ve faktuře musí adresát uplatnit u vystavovatele ve lhůtě 5 dnů od doruření faktury adresátovi. Právě dnes byla řádně doručena faktura, která obsahuje nesrovnalost v ceně fakturovaného zboží.

*Kdy počne běžet stanovená lhůta a kdy skončí, tedy do kdy je adresát oprávněn uplatnit své právo na odstranění nedostatků ve faktuře u druhé strany?*

## Právní domněnky a fikce

Zákon může podpůrně vycházet z existence nějaké skutečnosti, která není právem prokázána

**Právní domněnka** - zákon předpokládá existenci něčeho, o čem není jisté, že ve skutečnosti existuje

  - Vyvratitelná
    - Je připuštěn důkaz opak (*má se za to, že*)
  - Nevyvratitelná
    - Není připuštěn důlaz opaku (*platí, že*)

**Právní fikce** - je všeobecně známo, e fingovaná skutečnost neexistuje, nicméně se k ní z praktických důvodů přihlíží, jako by existovala

  - *Považuje se za to*

## Právní poměry

  - Jsou to společenské vztahy mezi dvěma a více konrétně určenými právními subjekty, které mají navzájem subjektivní práva a subjektivní povinnosti, které vznikají těmto subjektům přímo nebo zprostředkovaně na základě právním norem

### Předpoklady vzniku právních poměrů

1. Platná a účinná právní norma
2. Právní skutečnost
    - Musí nastat skutečnost (přip. více skutečností), se kterou právní norma spojuje vznik změnu nebo zánik právního poměru

### Prvky právních poměrů

1. Subjekty právniho poměru
    - Osoby v právním smyslu
2. Obsah právního poměru
    - SOuhrn práv a povinnosti vyplývajícíh daným subjektům z právního poměru
3. Obejkt právního poměru
    - to, čeho se týkají subjektivní práva a povinnosti právních subjektů daného právního poměru