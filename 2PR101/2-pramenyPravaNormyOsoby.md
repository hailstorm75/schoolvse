# Pojem a systém práva
## Právo objektivní a subjektivní
**Objektivní právo (právo v objektivním smyslu)**

  - Soubor obecně závazných pravidel chování stanovených nebo uznaných státem a státem vynutitelných

**Subjektivní právo (právo v subjektivním smyslu)**

  - Objektivním právem zaručená možnost chování subjektu (kterému obvykle odpovídá právní povinnost jiného právního subjektu)
  - Např. student dané univerzity má právo chodit na přednášky, ale není to jeho povinnost

## Prameny práva
  - Formy, v nichž je právo (právní normy) obsaženo

  1. Normativní právní akty
      - Právní předpisy
      - Vydává vždy orgán veřejné moci, které má k tomu právomoc
      - Má za cíl regulovat chování společnosti
  2. Normativní smlouvy
      - Ne všechny smlouvy jsou normativní
      - Obsahuji pravidla chování, která se obecně závazná
      - Typicky mezinárodní práva
  3. Soudní precedenty
      - Z anglosaských zemí
        - Rozhodnutí soudu, které bylo vydáno ve věci, která ještě dříve rozhodována a které jsou v budoucnu nadále závazná ve všech dalších rozhudnutích
        - V ČR soudní precedens nemá vliv na zákonik
  4. Právní obyčeje
        - Určité pravidlo chování, které je zachováváno v určité společnosti až se z něj stane právní obyčej
  5. Obecné právní zásady
  6. Jurisprudence
        - Odborné knihy, velmi staré svazky, které se pořád použivají jako promen práva ve Velké Británií

## Typy právní kultury

**Velké právní systémy:**

1. Kontinentálně evropský typ (kontinentální typ)
2. Angloamerický typ (anglosaský typ)
    - Velá Británie, USA, Austrálie, Nový Zealand, velká část Kanady
    - Převažují soudní precedenty

+ další (Islámský typ, smíčené typy ...)

## Prameny práva v ČR

1. Normativní smlouvy
    - Mezinárodní smlouvy
      - Vztahy mezi státy
      - Zavazují konkrétní státy
      - Výjimka podle čl. 10 Úst k jejíž dál Parlament ČR souhlas jsou součástí vnitrostátního právního řádu

2. Normativní právní akty
    - Ústavní pořádek ČR
        - Ústava
            - Není jednoduchý měnit ústavní zákony
        - LZPS
        - ... a další ústavní zákony
    - Zákony a zákonná opatření
    - Nálezy Ústavního soudu
    - Nařízení vlády
    - Vyhlášky ministerstv a jiných ústředních správních orgánů
    - Nařízení Rady obci a nařízení krajů
    - Obecné závazné vyhlášky obcí a krajů

## Prameny práva Evropské unie

**Primární právo EU:**

1. Smlouva o Evropské unii (SEU)
2. Smlouva o fungování Evropské unie (SFEU)
3. Smlouva o založení Evropského společenství pro atomovou energii (EURATOM)
4. Protokoly a přílohy ke smlouvám
5. Smlouvy o přístoupení členských států k Evropské unii
6. Ostatní smlouvy

**Předmět právní úpravy:**

  - Rozdělení pravomocí mezi Unii a členskými státy
  - Process rozhodování a legislativní proces
  - Pravomoci orgánů EU a rozsah jejich činnosti v rámci jejich oblasti kompetence

**Sekundární právo EU:**

1. Nařízení
2. Směrnice
    - Je závazná jenom pro členské státy
    - Členské státy mají povinnost ji tranponovat
        - Přizpůsobí vnitro-státní právní řád, aby splňoval směrnici
3. Rozhodnutí
    - Platí pro konkrétní subjekty (státy, osoby)
4. Stanoviska
5. Doporučení

# Právní normy

  - Obecně závazné pravidlo lidského chování, které je státem stanovené (přip. uznané) a státem vynutitelné

**Základní znaky:**

1. Normativnost
2. Obecnost
3. Formální určitost
4. Závaznost
5. Vynutitelnost

## Struktura právní normy

1. Hypotéza
    - Stanoví podmínky realizace **dispozice** (kdy nastane)
        - Koho se týká, v jakém čase a případě
2. Dispozice
    - Stanoví to samotné pravidlo
    - Vždy obsahuje příkaz, zákaz nebo dovolení
3. Sankční hypotéza
    - Podmínky udělení sankce
4. Sankce
    - Negativní důsledek porušení **dispozice**

## Druhy právních norem

  - Opravňujícíc (dovolující) X Zavazujícíc (přikazujícíc/zakazujícíc)
  - Kogentní X Dispozitivní
      - **Kogentní:** nepřipouští stranem se odchylit od kontrétní právní normy - právní norma platí a nelze ji měnit
          - "K zdánlivému právnímu jednání se nepřihlíží"
          - "Jednal-li někdo v omylu o rozhodujícíc okolnosti a byl-li v omyl uveden druhou stranou, je právní jednání neplatné"
      - **Dispozitivní:** připouští stranam aby se dohodli na něčem jiném
          - "Neurčí-li stanovy funkční období členů volebnýh orgánů spolku, je toto období pětileté"
          - "Neujednají-li strany, kdy má dlužník splnit dluh, může věřítel požadovat plnění ihned a dlužník je poté povinen splnit bez zbytečného odkladu"

  - Kolizní
      - Řeší rozpor mezi různými právními řády (např při obchodu se zehraničím)
  - Derogační (zrušení)
      - Zrušující právní norma
      - Ruší předchozí právní předpis
      - Něco jako override virtual bez volání bázi
  - Blanketová
      - Je součásti nějakého právního předpisu
      - Odkazuje na právní předpis, který se bude řešit v budoucnu
      - throw new NotImplementedExcetion();
  - Odkazujícící
      - Odkazuje na konkrétní pravidlo právního řádu
  - Výčet taxativní X demonstrativní
      - **Demonstrativní výčet** (příkladný)

## Platnost a působnost právních norem
### Platnost
Právní norma vydána přísluným orgánem v mezích jeho pravomoci, stanoveným postupem a řádně vyhlášena

Od **vydání** (publikace) do **zrušení** (derogace) právní normy

### Působnost právní normy
Vymezení rozsahu realizace a aplikace právní normy

1. Časová
    - Kdy počíná a kdy končí účinnost právní normy
2. Prostorová
    - Na kterém území je právní norma účinná
3. Osobní
    - Pro které osoby je právní norma účinná
4. Věcná
    - Pro jaké skutkové podstaty je právní norma účinná

#### Zpětná působnost právní normy

**Retroaktivita** - zpětná působnost právní normy

##### Právní retroaktivita
  - Právní norma je účinná v době, kde ještě není platná (neexistuje).
  - Použije se pozdější právní normy na případy, které se staly již v minulosti

*Příklad:*

> "Testnost činu se posuzuje a trest se ukládá podle zákona účinného v době, kdy byl čin spáchán. **Pozdějšího zákona se použije, jestliže je to pro pachatele přízněvější.**"

##### Nepravá retroaktivita
  - Vznik a platnost právního poměru vzniklého před účinnosti nového právního předpisu se řídí původní právní úpravou, obsah právního poměru se již posuzuji podle nové právní úpravy

*Příklad:*

> "Nájem se řídí tímto zákonem ode dne nabytí jeho účinnosti, i když ke vzniku nájmu došlo před tímto dnem; vznik nájmu, jakož i práva a povinnosti vzniklé přede dnem nabytí....KILL ME"

## Systém práva

TODO

## Osoby v právním smyslu

  - Subjekty práva se dělí na
      - Fyzické osoby - lidé
      - Právnické osoby

### Právní osobnost a svéprávnost

#### Právní osobnost
  - Způsobilost mít v mezích právního rádu práva a pivnnosti -> být účastníkem právního poměrů
  - Fyzická osoba (člověk): od narození do smrti
  - Právnická osoba: od svého vzniku do svého zániku

#### Svéprávnost
  - Způsobilost svým vlastním právním jednáním nabývat práv a zavazovat se k povinnostem -> způsobilost právně jednat

**Plná svéprávnost se nabývá:**

1. Zletilost
2. Uzavřením manželství
3. Přiznáním svéprávnosti

**Uzavřením manželství nezletilých soud musí**

  - Povolení soudu (ve výjimečných případech)
  - \> 16let
  - Důležité důvody (např. mateřství)

**Přiznání svéprávnosti soud musí**

  - Návrh nezletilého a souhlas zákonného zástupce (příp opačné)
  - \> 16 let
  - Schopnost se sám živit a obstarat si své záležitosti

**Omezení svéprávnosti**

  - Omezit může *soud*
  - stanoví opatrovníka
  - **Podmínky**:
      1. V zájmu člověka, jehož se týká
      2. Po jeho shlédnutí
      3. S plným uznáním jeho práv a jeho jedinečnosti
      4. Hrozila-li by mu jinak závažná újma
      5. Nějdéle na 3 roky

### Právnické osoby

**Právnická osoba** - organizovaný útvar, o kterém zákon stanoví, že má právní osobnost, nebo jehož právní osobnost zákon uzná

**Právní osobnost**

  - Mohou vystupovat v právních vztazích (poměrech)
  - Mají vlastní majetek oddělený od majetku svých členů
  - Mohou právně jednat prostřednictvím svým zástupců
  - Mohou vystupovat jako účastníci správních a soudních řízení

#### Klasifikace právnických osob

  - Korporace
      - Založená na společenství osob
  - Fundace
      - Založená na společenství majetku
      - Dělí se na 
          - Nadace
          - Nadační fondy
  - Ústavy
      - Smíšené společenství osob a majetku

#### Ustavení a vznik právnických osob

**Ustavení:**

  - Zakladatelským právním jednáním
  - Zákonem
  - Rozhodnutím OVM
  - Jiným způsobem (zvl. předpis)

**Zakladateslké právní jednání:**

1. Název
2. SÍdlo
3. Předmět činnosti
4. Statutární orgán

Všechno musí být uvedeno v písemné formě.

**Vznik:**

  - Zápisem do veřejného rejtříku

#### Jednání právnické osoby

**Nemá svéprávnost**

#### Zrušení právnické osoby

1. Právním jednáním
2. Uplynutím doby
3. Dosažením učelu
4. Rozhodnutím OVM -> soud zruší
    1. Nezákonná čínnost PO
    2. Již nesplňuje přepoklady vzniku
    3. \> 2 roky není usnášeníschopná