# Jednání přímé a nepřímé

**Přímé:** - Jedná sama osoba

- pouze človek - fyzická osoba
- právnická osoba sama jednat nemůže - nemá vlastní vůli

**Nepřímé:** - Za osobu jedná její zástupce neboli **zastoupení**

## Zastoupení

Rozsah oprávnění jednat za jiného - **zástupčí oprávnění**

**Zastoupený = zmocnitel** <-> **zástupce = zmocnitel**

- Kdo jiného zastupuje, musí dát najevo, co ho k tomu opravňuje
- Ze zastoupení vznikají práva a povinnosti přímo zastoupenému
- Pokud z jednání nevyplývá, že někdo jedná za jiného, platí, že jedná vlastním jménem

### První typ dělení

#### Přímé zastoupení

- Zástupce (zmocněnec) jedná na účet a **jménem zastoupeného (zmocnitele)**

#### Nepřímé zastoupení

- Zástupce jedná na účet zastoupeného **vlastním jménem**

Zastoupený -> Zástupce -> Třetí osoba

### Druhý typ dělení

- Zákonné
- Smluvní
  - Vzniká na základě smlouvy/dohody
- Na základě rozhodnutí soudu
  - Třeba soud zvolí opatrovníka

#### Smluvní zastoupení

**Smlouva o zastoupení** - oboustranná dohoda zmocněnce a zmocnitele:

1. Že jeden druhého zastupuje
2. Jakým způsobem
3. V jakém rozsahu
4. Po jakou dobu
5. Při jakých právních jednáních

> **Plná moc** - jednostranné právní jednání, dokládající existenci zastoupení a jeho rozsah

Zvláštní typ smluvního zastoupení -> **prokura**.
Je vyhrazeno podnikatelům - musí mít zápis v obchodním rejstříku.

Zmocněnec **překročí zástupčí oprávnění** + zmocnitel nesouhkasí -> BZO poté, co se o takovém jednání dozvěděl, oznámit osobě, se kterou zmocněnec právně jednal.

Pokud by to neučinil -> překročení oprávnění schválil

(Netýká se případů, kde osoba, se kterou zmocněnec právně jednal, měla a mohla z okoností bez pochybnosti poznat, že zmocněnec... TODO )

#### Důvody zániku zmocnění

1. Vykonání právního jednání, na které bylo zastoupení
2. Odvolán zmocněnec
3. Výpověď zmocněnec
4. Smrt (příp. zánik) zmocnitele nebo zmocněnce (nedohodnou-li se strany v rámci dohody o zastoupení jinak)

### Zákonné zastoupené

> Na základě zákona -> není již potřeba nic dalšího

- **Zastoupené nezletilého dítěte**
  - Rodiči
  - Poručníkem
- **Vzájemné zastupování manželů**
- **Zastoupení právnických osob**
  - Členem statutárního orgánu
  - Členem orgánu nebo zaměstnancem

#### Zastoupení dítěte

> Rodiče mají povinnost (a zároveň právo) zastupovat dítě při právních jednáních, ke kterým **není právně způsobilé**

Pravidla:

- Oba rodiče **zastupují především společně** - ve shodě (samotný akt jednání může provést každý samostatně)
- Pokud samostatně -> vyvratitelná právní domněnka, že rodič jedná se souhlasem druhého rodiče
- Rozpor mezi rodiči -> soud (rozhodne dle zájmu dítěte)

#### Poručenství

> Není žádný z rodičů, který má a vůči svému dítěti vykonává rodičovskou odpovědnost v plném rozsahu

- Má vůči dítěti zásadně všechny povinnosti a práva jako rodič

#### Vzájemné zastupování manželů

> Manžel má právo zastupovat svého manžela **v jeho běžných záležitostech**

1. **Běžně záležitosti rodina:**
  - Právní jednání jednoho manžela zavatuje a opravňuje oba manžele společně a nerozdílně
2. **Ostatní záležistosti rodiny:**
  - Jen dal-li druhý manžel k právnímu ...TODO

SItuace, kdy manžel zástupčí právo **nemá:**

- Jeden z manželu si sám nepřeje, aby byl druhým manželem zastupován

1. **Návrh na soud**, aby zástupčí právo druhého manžela úplně zrušil
2. **Nesouhlas se zastupováním** - manžel, předem sdělí tomu, s nímž jeho manžel má .. TODO

#### Zastoupení právnícké osoby

1. **Členem statutárního orgánu**
    - Nejšírší, tzv generální zástupčí oprávnění
    - Člen statutárního orgánu je oprávněn zastupovat právnickou osobu ve všech věcech

2. **Členem právnické osoby, jejím zamšstnancem nebo členem jejího orgánu**
    - Oprávnění zastupovat v rozsahu obvyklém vzhledem k jejich zařezení nebo funkci

#### Zákonné zastoupení podnikatele

**4 typy zákonného zastoupení podnikatele:**

1. Členem statutárního orgánu
2. Vedoucím odštěpného závodu
3. POvěřenou ... TODO vole už se na to vyseru

#### Zastoupení na základě rozhodnutí soudu

SOud jmenuje osob (opatrovanci) zástupce:

- Je-li to potřeba k ochraně jeho zájmů
- Vyžeduje-li to veřejný zájem

- **Opatrovník**
  1. TOmu, jehož ve svéprávnosti omezil
  2. TOmu, o kom není známo, kde pobývá
  3. Neznámému člověku zúčastněnému při určitém právním jednání
  4. Tomu, jehož zdravotní stav mu působí obtíže při správě jmění nebo hájení práv
- **Opatrovník právnické osoby**
  - Z... hej twl on ten slide ukazoval jen 2 sekundy HELP
- **Kolizní opatrovník**
  - Při střetu zájmu (kolizí) zákonného zástrupce nebo opatrovníka se zájmem zastoupeného (či zastoupených mezi sebou)
  - jmenuje soud zastoupenému kolizního opatrovníka
  - také pokud tento střed hrozí

# Věci v právním smyslu a věcná práva

Např:

- Pozemek
- Auto
- Pohledávka

## Pojem věci

Vše, co je rozdílné od osoby, a slouží potřebě lidí

- Užitečné
- Ovladatelné

**Věci není:** osoba, lidské tělo ani jeho části, živé zvíře

### Rorzdělení věcí

- Hmotné X Nehmotné
- Nemovité X Movité
- Zastupitelné X Nezastupitelné
- Zuživatelné X Nezuživatelné

## Věcná práva

### Třídění subjektivních práv

- Práva
  - Osobní
    - Osobní (statusová)
    - Osobnostní
  - Majetková
    - Zá.. TODO

### Charakteristické rysy věcných práv

1. Předmětem věcných práv je **věc**
2. **Absolutní** práva: působí vůči všem
3. Věcná práva "**lpí**" na věci

# Vlastnictví

## Vlastnické právo

> první a základní, nejčastěji se vyskytující věcné právo

**Právní panství nad věci** VS Faktické panství nad věci (držba)

**Vlastnické právo** - právo vlastnika nakládat se svým vlastnictvím v mezích právniho řádu libovolně a jiné osoby z nakladani vyloučit

### Nabývání vlastnického práva

> Vznik subjektivního vlastnického práva určité osobě (vlastníkovi)

Musí existovat **právní důvod** (titul):

1. Právní skutečnost - právní jednání, konstitutivní
2. ... TODO

#### Způsoby nabytí

**Původní** (originární) - nabytí vlastnictví k věci, která dosuk vlastníka neměla (viz absolutní) nebo k nabytí dojde nezávisle na vlastnictví dosavadního vlastnika

**Odvozené** (derivativní) - nabytí od původního vlastníka přechodem (dědicvtí) nebo převodem vlastnického práva (smlouva)

#### Omezení vlastnického práva

> Pozor' VLastnické právo je sice nejméně omezené (všeobecné), ale **není neomezené**

Vlastník má nejen práva, ale i povinnosti

**Omezení:**

- Zákonná (*ex lege*)
  - Věřejnoprávní
    - Stanové veřejnoprávní předpisy
  - Soukromoprávní
    - Stanoví NOZ - zakázaná a přikázaná jednání vlastnika
- Smluvní (*ex contractu*)
- Na zákldě rozhodnutí OVM (soudu, správního orgánu)

Obecně **zakázáno**:

1. Nad míru přiměrenou poměrům zývěžně rušit práva jiných osob
2. Vykonávat takové činy, jejichž hlavním účelem je jinou osobu obtěžovat nebo poškodit
  - tzv. šikana

**Imise (tzv. účinky)** - cokoli, co uniká z nemovitosti navenek na pozemek souseda **v míře nepřiměřené mistnim pomerum a podstatne** to omezuje obvykle uživani sousedniho pozemku

Přisnější právní úprava pro tzv. **přímé** imise:

- Zakázano bez ohledu na přiměřenost a stupeň omezení souseda

#### Sousedská práva

> Právní regulace slouží přimých sousedů s cílem vyrovnat jejich práva a povinnosti (jednoho vůči druhému)

1. Cizí movitá věc na sousedově pozemku
2. Přepadlé plody, přesahující větve a kořeny
3. Vysazovaní stromů a stavění staveb v blízkosti hranice pozemku
4. OPravy staveb ze sousedního pozemku

Vlastník pozemku musí zakona strpet určité jednání vlastnika sousedniho pozemku

#### Podmínky vyvlastnění či omezení vlastnického práva

1. Veřejný zájem, který nelze uspokojit jinak
2. Zákonný podklad (zákon stavební, elektrizační, o plynovodu, teplovodu atd.)
3. Plná náhrada odpovidající míře dotčení vlastnického práva

### Ochrana vlastnických práv

1. Vlastník má právo na vydání své věci od toho, kdo ji neoprávněně zadržuje

> Musí požadovat soud -> žaloba na vydání (reivindikační)

- Žádá vydání věci
- Věc musí být určena zcela přesně

2. Nekdo jinak zasahuje do vlastnickho prava nebo rusi vlastnicke pravo - žáloba zápůrčí (negotorní)

> Žádá, aby ten, kdo od jeho vlastnickho prava neopravnene zasahuje:

- Zasahovat nebo rusit prestal
- Odstranil nasledky -> navratil veco do puvodniho stavu (restitutio in integrum)

### Spoluvlastnitvi

> Dve a vice osob vlastni jednu vec

Subjekty spoluvlastnitvi - spoluvlastnici (spolecnici)

Rozlisuji se na

- Realne
  - Podil je realne vymezen
- Idealni
  - Podily jsou pouze myslene
- Smisene
  - Napr bytove spoluvlastnictvi

#### Rozhodovani o spolecne veci

Jednotna vule - vysledkem rozhodovani (hlasy podle podilu)

1. Bezne zalezitosti - princip majority
2. Vyznamne zalezitosti - 2/3 vetsina
3. Jine zatizeni

#### Bytove spoluvlastnictvi

> Smiseny druh spoluvlastnictvi

Dum rozdeleny na jednotky - zapis do katastru nemovitosti

**Jednotka** - Vec nemovita - Soubor

1. Bytu jako realne vymezene casti domu
2. Idealniho podilu na spolecnych castech nemovite veci (viz. pozemku pod domem)

#### Pridatne spoluvlastnictvi

> min. 2 veci a k nim spolecne nalezi dalsi vec, ktera slouzi spolecnemu uzivani tech jednotlivych veci

Priklady:

- Prijezdova cesta k oplocene osade chat
- Studna slouici vice domum
- Spolecna trafostanice
- Garazovy dum pro uzavrenou vilovou ctvrt

### Zruseni spoluvlastnictvi

**Zakladni principy**

- Nikdo nemuze byt nucem setrvavat ve spoluvlastnictvi
- Zruseni spoluvlastnictvi

**Zpusoby**

... TODO

#### Rozhodnuti soudu o vyporadani spoluvlastnictvi

Neni dohoda - soud na zaklade zaloby kterehokoli rozhodne

1. Rozdeleni veci
2. Prikaze jednomu spoluvlastnikum za nahradu
3. Nechce-li nikdo - verejna drazba a rozdeleni vydelku
4. TODO

# Drzba

## Nevlastnicke pravni stavy

2 nevlastnicke pravni stavy

1. **Drzba** (*possessio*) - fakticky stav, chraneny statem
2. **Detence** (*detentio*) - stav, kde nekdo ma vec u sebe, zpravidla z vule vlastnika

## Ucel drzby

1. Chranit (vedle pravniho stavu) take fakticky stav
2. Umoznit legitimaci

## Podminky drzby

1. Drzitel (objektivni slozka) musi mit faktickou moc nad veci
  - Moznost a schopnost vec ovladat (ale staci mit ve sfere vlivu - napr. mam klice od auta)

2. Drzitel (subjektivni slozka) musi umysl mit vec vylucne pro sebe - pro svuj prospech

## Kvalifikovana drzba

> Takova drzba, ktera je chranena zakonem jako drzba

1. Radna
2. Poctiva
3. Prava

> Ma se za to, ze drzba je radna, poctiva a prava

### Radna drzba

> Zaklada se na platnem pravnim duvodu

Platny pravni duvod

1. Kdo se ujme drzby primo, bezprostredne, nezprostredkovane veci nici, prip. zjevne opustene
2. Odvozeny od p ... TODO

### Poctiva drzba

> Ma-li ten, kdo drzby nabyl, z presvedciveho duvodu za to, ze mu pravo, ktere vykonava, nalezi

**Poctivost = dobra vira**

### Prava kvalifikovana drzba

Neprava = drzba nabyta "neferove"

1. Svemocne
2. POtaji
3. Lsti
4. "Pouha" vyprosa

Sry jako na zbytek pekne seru
