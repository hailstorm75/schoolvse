# Úvod do práva
## Literatura
  - Základy Práva Pro Neprávníky po rekondifikaci soukromého práva: Michael SPirit a kolektiv (4. aktuální vydání)
  - Základy obchodního práva po rekondifikaci soukromého práva - Zbyněk Švarc a kolektiv (4. upravené vydání)
  - Ústava 1/1993 Sb
  - Listina zakladnich lidskych prav 2/1993 Sb
  - 89/2012 Sb

# Stát a ústavní systém ČR
  - ČR je:
    - Parlamentní republika
    - Stát
        - Svrchovany a vyluicny
        - Jednotny (unitarni)
            - Není rozdělen na vícero dílčích států
        - Demokraticky
        - Právní
            - Založen na principu zákonosti
            - Právo nezavazuje tomu, komu je určeno, ale tomu kdo ho vytvořil
        - Sekulární
            - Stát se nespojuje nějaké náboženství

## Princip dělby moci
### Vertikální dělba moci

1. Územní uspořádání (krajská a obecní samospráva)
2. Vztah k mezinárodním organizacím (EU)

### Horizontální dělba moci
> Charles de Montesquieu

1. Zákonodárná
2. Vykonná
3. Soudní (+ moc kontrolní, bankovní)

Vyvážení mocí - tyto moci se navzájem kontrolují, systém brzd

### Dělba moci ve státě
#### Zákonodárná moc
  - **Právní úprava**:
    1. Ústava ČR (1/1993 Sb.)
    2. Zákon o jednacím řádu Poslanecké sněmovny (90/1995 Sb.)
    3. Zákon o jednacím řádu Senátu (107/1999 Sb.)
    4. Zákon o střetu zájmů (159/2006 Sb.)
    5. Zákon o sídle Parlamentu ČR (59/1996 Sb.)
    6. Ústavní zákon o bezpečnosti ČR (110/1998 Sb.)
  - Parlament ČR:
      - Poslanecká snemovna + Senát

|                | **Poslanecká sněmovna**                        | **Senát**                                                 |
| -------------- | ---------------------------------------------- | --------------------------------------------------------- |
| Složení        | 200 poslanců                                   | 81 senatoru                                               |
| Funkcni obdobi | 4 roky                                         | 6 let                                                     |
| Volby          | Každé 4 roky - **Systém poměrného zastoupení** | Každé 2 roky 1/3 senátorů - **Většinovy systém (2 kola)** |
| Volebni pravo  | Aktivní = 18 let, Pasivní = 21 let             | Aktivní = 18 let, Pasivní = 40 let                        |

**Vznik mandátu poslance a senátora:** zvolením (čl. 19 odst. 3 Úst)

**Zánik mandátu:**

1. Smrtí
2. Odepřením slibu nebo složením slibu s vyhradou
3. Uplinutím volebního období
4. Vzdáním se mandátu
5. Ztrátou volitelnosti
6. Vznikem neslučitelnosti funkce
7. U poslanců rozpuštěním poslanecké sněmovny

##### Pravomoci Parlamentu ČR

1. Zákonodárná pravomoc
2. Pravomoc vyjadřovat souhlas k ratifikaci mazinárodních smluv
3. Pravomoc vyjadřovat se k připravovanym rozhodnutim EU
4. Pravomoc Poslanecké Sněmovny příjmat státní rozpočet
5. Kontrolní a kreační pravomoci Parlamentu a jeho komor
6. Pravomoci souviejícíc s bezpečnosti státu

Prezident republiky svolává schůze sněmovny/senátu (první nejpozdějí 30. den po dni voleb)
Zasedání sněmovny/senátu lze přerusit na maximálně 120 dnu v roce.
V případě, že bylo přeruseno na vice jak 120, může to byt důvodem pro rozpuštění u poslanecké sněmovny.

##### Rozpuštění Poslanecké sněmovny
Prezident může rozpustit PS:

1. PS **nevyslovila důvěru** nové vládě, jejiž předseda byl prezidentem jmenován na **návrh předsedy** PS
2. PS se neusnese do 3 měsiců o vládním návrhu zákona s jehož projednáním **spojila vláda otázku důvěry**
3. zasedání PS bylo **přerušeno** na více jak 120 dní v roce
4. PS **není usnášenischopná** na více jak 3 měsíce

  - Prezident musí rozpustit navrhne-li to PS (3/5 všech poslanců)
  - Nelze rozpustit 3 měsíce před koncem volebního období
  - Senát zastupuje parlament během jeho rozpuštění prostřednictvím zákonného opatření
    - Nemůže měnit ústavní zákony ani volební zákony
    - Na první schůzi senát předává zákonná opatření PS

Návrhují zákony:

  - Poslanec
  - Skupina poslanců
  - Senát
  - Vláda
  - Zastupitelstvo kraje

O návrhu zákonu rozhoduje pouze senát, nikoliv vláda.
Vláda ale pořád se může vyjádřit bud kladně, nebo záporně k návrhu zákonu.

Poslanecká sněmovna může zvrátit rozhodnutí senátu (je-li to zamítnutí) pokud bude nad poloviční počet hlasů.

Po přijetí zákona, dále rozhoduje prezident republiky.
Pokud vrátí zákon poslanecky snemovne, musejí odhlasovat nad polovinu pro zvrácení.
Zákon nemusí byt podepsan prezidentem k tomu, aby platil. Nevráti-li podpis, automaticky zákon platí.

Legislakační doba

  - Doba mezi platnosti a učinnosti zákona
  - Je to k tomu, aby se osoby, kterych se dany zakon tyka mohli k nemu pripravitn
  - Nejmenší doba může byt 0 dnů - platnost a učinnost je tentyž denb

#### Vykonná moc
##### Prezident republiky
  - Je hlavou státu
  - Z vykonu je neodpovědny
  - Volen v přímych volbach obcany
  - Prezident je neodvolateny pokud:
    - Není schopen ze závažnych důvodů úřad vykonávat
    - Úřad se uvolní

**Právní úprava**:
1. Ústava ČR - Hlava |||. Moc konná
2. Zákon o bezpečnosti ČR
3. Jednací řád Poslanecké sněmovny
4. Jednací řád Senátu
5. Zákon o Kanceláři prezidenta republiky

###### Funkce prezindeta republiky
**Vznik**:
  - Složením slibu do rukou předsedy Senátu

**Zánik**:
1. Uplynutím volebního období
2. Vzdáním se úředu do rukou předsedy Senátu
3. Smrtí
4. Rozhodnutím Ústavního soudu, že se dopustil velezrady nebo hrubého porušení Pstavy a ztrací prezidentsky úřad i způsobilost znovu jej nabyt

###### Pravomoci prezidenta republiky

**Bez kontrasignace**

1. Jmenuje a odvolava vlasu a prijma jeji demisi
2. Svolava zasedani Poslanecke snemovny
3. Rozpousti Poslaneckou snemovnu
4. Jmenuje soudce Ustavniho soudu
  - Senat musi odsouhlasit navrh
  - Jsou jmenovany na dobu 10 let
5. Jmenuje ze soudcu predsedu a mistopredsedy Nejvyssiho soudu
6. Odpousti a zmirnuje tresty ulozene soudem (agreciace) a zahlazuje odsouzeni (rehabilitace)
7. Suspenzivni veto
8. Podepisuje zakony
9. Jmenuje prezidenta a viceprezidenta NKU
10. Jmenuje cleny Bankovni rady CNB

**S kontrasignaci**

1. Zastupuje stat navenek
2. Sjednava a ratifikuje mezinarodni smlouvy
3. Je vrchnim velitelem ozbrojenych sil
4. Jmenuje a povysuje generaly
5. Prijma vedouci zastupitelsych misi
6. Poveruje a odvolava vedouci zastupitelskych misi
7. Vyhlasuje volby do Poslanecke snemovny a do Senatu
8. Udeluje statni vyznamenani
9. Jmenuje soudce
10. Narizuje, aby se tretni rizeni nezahajovalo, a bylo-li zahajenom aby se v nem nepokracovalo (abolice)
11. Ma pravo udelovat amnestii (prominuti trestu nejake skupine odsouzenych)

###### Odpovednost prezidenta republiky

Po dobu vykonu jeho funkce jej nelze:
1. Zadrzet
2. Trestne stihan
3. Stihat pro prestupek nebo jiny spravni delikt

##### Vláda
  - Vrcholny organ vykonne moci
  - Odpovedna Poslanecke snemovne

Vlada vznika po volbach v poslanecke snemovne.
Prezident jmenuje predsedu vlady

Predseda lasy navrhne prezidentu ostatni cleny vlasy.
Prezident jmenuje ostatni cleny vlady.

Do 30 dnu se musi podat zadost o vysloveni duvery vlade.
Parlament musi odsouhlasit vic jak polovina.
Pokud zamitnou, prezident musi znovu zvolit predsedu (muze byt stejny jako puvodne) a cely process se opakuje.
Pokud se zamitne jiz zas, prezident jmenuje na navrh poslanecke snemovny.
Pokud zase odmitnou, prezident ma pravo rozpustit poslaneckou snemovnu

###### Předseda Vlády

Klíčové postavení:

  - Organizuje činnosti vlády
  - Řídí její schůze
  - ...

1. Kontrasignuje rozhodnuti prezidenta republiky
2. podepisuje pravni predpisy
3. Navrhuje jmenovani a odvolani ministru
4. Navrhuje povereni mistru rizenim ministerstev/jinych uradu

#### Soudní moc
Rozhodovací pravomoc - nadřazené postavení

**Soudní soustava v ČR**

1. Nejvyšší soud (Brno) + Nejvyšší správní soud (Brno)
2. Vrchní soud (Praha, Olomouc)
3. Krajsky soud (Mestsky soud v Praze)
4. Okresní soud (Obvodní soudy v Praze, Městsky soud v Brně)

> Městsky soud v Praze a Brně nejsou na stejné úrovni
> Ustavní soud (Brno) - soudní organ ochrany ustavnosti. Stojí mimo tuto soustavu

###### Podmínky jmenování soudcem

1. Bezuhonnost (odsouzen za trestny čin)
2. Občan CR
3. Vysokoškolské právnické vzdělání v ČR (stupeň Mgr)

###### Vznik a zánik funkce soudce

**Vznik:** jmenováním do funkce prezidentem republiky

**Zánik:**

1. Dovršení 70 let věku
2. Vzdání se funkce do rukou prezidenta republiky
3. Nezpůsobilost vykonávat funkci
4. Ztráta bezúhonnosti
5. Kárné opatření odvolání z funkce
6. Omezení svéprávnosti
7. Pozvytí státního občanství
8. Smrt

##### Ustavní soud

**Rozhoduje o**

- Zrušení zákonů (ustanoveni) pro rozpor s ust. pořádkem
- Jinych pravnich predpisu pro rozpor s ústavou či zákonem
- Ústavni stižnosti orgánu uzemní samosprávy proti nezakonnému zásahu státu
- Ústavni stižnosti proti pravnomocnemu rozhodnuti a jinemu zasahu OVM do ustavne zarucenych ZPS

**Delka funkčního období ústavních činitelů v ČR**

| Činitel                              | Doba                                      |
| ------------------------------------ | ----------------------------------------- |
| Poslanec                             |  4 roky                                   |
| Senator                              |  6 roky                                   |
| Člen vlady                           |  4 roky                                   |
| Prezident republiky                  |  5 let                                    |
| Soudce                               | bez časového omezemí, maximálně do 70 let |
| Soudce Ústavního soudu               | 10 let                                    |
| Prezident NKÚ                        |  9 let                                    |
| Guvernér a člonové Bankovní rady ČNB |  6 let                                    |

Organy Evropske unie

EU je upravena v jejim fungovanim ve dvou smlouvach

  - smlouva o EU
  - smlouva o fungovani EU
    - soucasti je rozpis organu EU
      1. Evropska rada
          - nejvyznamnejsi orgna, vystupuji v ni nejvyssi clenove reprezentujici staty
          - Rozhoduje o zakladnich otazkach smeru EU
          - schazi se 2x v jednom pololeti - Euro summit
      2. Rada
          - Schazi jednotlivy ministri reprezentujici konkretni sekce - treba ministri financi
          - ma na starost vsechny zasadni otazky EU
      3. Evropsky parlament
          - CLenove: 751 poslancu, voleni ve volbach v jednotlivych clenskych statech
          - Funkcni obdobi 5 let
          - ROzhodovani: Podili se na legislativni cinnosti
      4. Evropska komise
          - Je to vykooni organ
          - Clenove jsou Euro komisare
          - Haji zajmy unie jako celku a ne sveho statu
          - Funkcni obdobi 5 let, zaseda neverejne 1x tydne
      5. Soudni dvur Evropske unie (SDEU)
      6. Evropska centralni banka
      7. Ucetni dvůr

