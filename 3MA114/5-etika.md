# Etika

> Materiál Národní ekonomické rady vlády z roku 2011 odhaduje, že neefektivně je v Česku vynaloženo 10% z celové výše veřejných zakázek, 64 miliard korun

- Neefektivně využité peníze

> Podle průzkumu, který prováděla agentura Gfk, zažilo v šikanu na pracovišti v CZ 16% lidí. Mobbing může vést až k sebevraždě (odhaduje se, že až 20% ze všech sebevražd)

- Mobbing není nelegální...
- Když nedojde k smrti tak to nikdo neřeší

> $1,227, that's how much a study sponsored by Philip MOrris said the Czech REpublic saves on healthcare, pensions and housing every time
> Studie EY z roku 2016 ukazuje, že díky šedé ekonomice přicházíme ročně o 109mld Kč/rok

## Etické z pohledu užitku - utilitarismus

*Jeremy Bentham a John Stuart Mill*

> Etické je to, co je nejlepší pro co největší počet lidí

## Etické z pohledu jednotlivce - individualismus

> Sledujede, co jednomu člověku nějaké rozhodnutí způsobí v dlouhodobém horizontu. Seberegulace chování ve prospěch dlouhodobých cílů

- Vyplatí se vám podvádět při docházce nebo testu?

- Pohled morálního práva - Nedojde při rozhodnutí či chování k porušení něčích základních lidských práv?
- Pohled spravedlnosti - Je rozhodnutí nebo chování spravedlivé a nestranné?
- Pohled užitku - Je rozhodnutí nebo chování tím nejlepším pro většinu lidí?
- Pohled jednotlivce - Podpoří rozhodnutí nebo chování moje dlouhodobé zájmy?
