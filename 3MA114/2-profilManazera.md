# Základní kompetence manažera
## Souhrn dvou dimenzí
  - Odbornost
    - Odborné znalosti, jež manažer získává vzděláváním a zkušenostemi z řešení různych praktickych situací
  - Chování
    - Jednání, vystupování v dané organizaci i mimo ní

## Odbornost
  - **Školní příprava:**
    - střední, VŠE, MBA, Ph.D.
  - **Sebevzdělání:**
    - Vzdělávácí programy (MOOCs, Khan academy)
    - Proaktivní sebe-vzdělávání formou vyhledávání, vstřebávání a využití novych poznatků
  - **Praxe:**
    - Dána praxi a tréninkem
    - Best management practices (plánování, delegování, ...)

## Principy práce manažera

  - **Priority - Parentovo pravidlo** (80/20)
    - Je důležité rozlišovat, co je vyznamné, co je prioritou činnosti a na co se zaměřit
    - Stejně důležité je určit co naopak důležité není a co nebude předmětem zájmu -> čemu se nevěnovat vůbec
  - **Prevence**
    - Čím dříve se objeví a odstraní nedostatek, tím nižší budou ztráty s ním spojené
    - Pravidlo "deseti"
    - Bezpečnost práce (BOZP)
    - Udržitelnost, kvalita

# Styl manažerské práce
## Rensis Linkert (1903 - 1981)

  - **Autokraticky**
    - Direktivní úkolování
    - Bez zapojení podřízenych
    - Bez snahy nastolit důvěru
  - **Benevolentní**
    - Sice Autokrativní
    - Zeptá se
    - Odměny a tresty
    - Základní důvěra v podřízení
  - **Konzultativní**
    - Obousměrná komunikace
    - Rozhodování "nahoře"
    - Zejména odměny
    - Rozhodování s podřízenym
  - **Participativní**
    - Podřízeni zapojení do rozhodování
    - PLná důvěra v podřízené
    - REalizace na ...

# Vztah manažera k podřízenym
## Autorita manažera (vážnost)

  - **Autorita formální**
    - Dáma postavením v org. Chartu
    - Přídělené pravomoci/odpovědnosti
  - **Neformální autorita**
    - Uznání schopnosti, chvání, jednání s podřízenymi

Posilování:

  - Pečovat o svou kvalifikaci, uznat zásluhy druhych
  - Dbát o pracovní morálku, spravedlnost, konzistenci
  - Příkladnost v morálce, rovny přístup, uznání

## Prezentace
  - Nutná součást komunikace k zaměstnancům klientům ...
  - Vyjasnění cíle:
    - Co, komu, čas, kde, technika ...
  - Posluchač je schopen intenzivně vnímat 10-20 min
  - Image prezentujícího, klid, důraz, sebevědomí
  - Struktura, přehlednost, jasné poselství
  - Reakce autiroia
  - Elevator pitch

# Sebeřízení
  - Sebepoznání
    - Kdo jsem
    - Co umím
    - Co dovedu
    - Drucker:
      - DOstat se z neschopnosti na průměr
      - DOstat se z vyborné úrovně na vynikající
    - Změny ve stresu
  - Seberozvoj
    - Po identifikaci toho co změnit, za tím jít
    - Sebevzdělávání, průběžny posun dopředu
    - Schopnost jednání s druhymi
    - Reprezentace firmy
  - Sebehodnocení
    - Sebekontrola jako nátroj uvědomění si chyb a jejich korekce
    - Drucker:
      - Zaznamenávat si rozhodnuti a po čase se k nim vracet a vyhodnocovat

## Řízení času
  - Všem měřen stejně
  - Každy dokáže využít jinak
  - Prioritizace
  - Identifikace irelevantních úkolů
  - Paretův princip (80/20) prioritizace + naléhavost
    a. Vyznamné úkoly (zařadit do programu - řešit)
    b. Středně důležité (delegování, hrubé rozhodnutí, podějí)
    c. Irelevantní (neřešit)

  - Soustředění se na činnost
    - Věnovat se pouze jedné věci po delší dobu
  - Člověk není schopen s plym nasazením pracovat na více věcech najednou - multitasking byl vyvrácen (jen 2,5% populace je schopno multitaskingu)
  - Biorytmus - individuální:
    - Vyšši vykonnost uprostřed tydne (ne Po a Pá)
    - Vykonnostní křívka

