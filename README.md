# Description
VŠE subject notes and exam preparations

# Downloading
All documents will be available in the **.pdf** format

To download the documents follow these steps:

1. Navigate to CICD/[Pipelines](https://gitlab.com/hailstorm75/schoolvse/pipelines) page in the sidebar
2. A list of pipelines will open
3. Find the entry with the latest tag (usually found in the pipeline column)
4. Documents can be downloaded as **.zip** by clicking the download icon on the far right

If you wish to preview the files before downloading please follow these steps:

1. Navigate to CICD/[Jobs](https://gitlab.com/hailstorm75/schoolvse/-/jobs) page in the sidebar
2. A list of jobs will open
3. Click the first entry (click either the **Passed** label or the job id which is a 9 digit number)
4. A build output log will open
5. In the right sidebar find the **Job artifacts** heading
6. Click the **Browse** button under the heading to browse generated output

> Note: Each download is available for a week. After expiration it is deleted to preserve space

# Contributing
If you would like to contribute to this repository, please request the repository owner to provide you with the required priviliges and your own branch.

## Contribution rules
  - Each subject has its own folder with the given subject code as the folder name.
  - Documents containing either lecture notes or seminar notes must begin with to match the order in which the material was presented
  - Document names must contain only ANSI charaters
  - Documents can either be **.md** or **.tex**

